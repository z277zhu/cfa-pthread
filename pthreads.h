#pragma once


extern "C"{
    int pthread_getconcurrency( void );
    int pthread_setconcurrency( int new_level );
}
