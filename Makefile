#macros
OUTPUT:=PNOOUTPUT
MAIN:=PNOMAIN
CFA=cfa
CFAFLAGS = -g -Wall -D"${OUTPUT}" -D"${MAIN}" -O0

.PHONY : all clean

all: pthread

pthread: pthread.cfa
	rm -rf pthread
	${CFA} ${CFAFLAGS} $^ -o pthread

create_demo: pthread.cfa tests/pthread_demo_create_join.cfa
	rm -rf create_demo
	${CFA} ${CFAFLAGS} $^ -o create_demo

lock_demo: pthread.cfa tests/pthread_demo_lock.cfa
	rm -rf lock_demo
	${CFA} ${CFAFLAGS} $^ -o lock_demo

cond_demo: pthread.cfa tests/pthread_cond_test.cfa
	rm -rf cond_demo
	${CFA} ${CFAFLAGS} $^ -o cond_demo

attr_demo: pthread.cfa tests/pthread_attr_test.cfa
	rm -rf attr_demo
	${CFA} ${CFAFLAGS} $^ -o attr_demo

key_demo: pthread.cfa tests/pthread_key_test.cfa
	rm -rf attr_demo
	${CFA} ${CFAFLAGS} $^ -o key_demo

buffer: pthread.cfa tests/bounded_buffer.cfa
	rm -rf buffer
	${CFA} ${CFAFLAGS} $^ -o buffer

gethostbyname: pthread.cfa  tests/gethostbyname.cfa
	rm -rf gethostbyname
	${CFA} ${CFAFLAGS} $^ -o gethostbyname

once_demo: pthread.cfa tests/pthread_once_test.cfa
	rm -rf once_demo
	${CFA} ${CFAFLAGS} $^ -o once_demo

clean :						# remove files that can be regenerated
	rm -f pthread create_demo cond_demo lock_demo buffer gethostbyname attr_demo key_demo once_demo
