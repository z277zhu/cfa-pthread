# CFA-pthread

pthread emulation by cforall user-level threads


## Main File
- pthread.cfa
- pthreads.h

## Build
1. git clone ist-git@git.uwaterloo.ca:z277zhu/cfa-pthread.git
2. make sure cfa is installed 
3. make lock_demo
4. ./lock_demo

## Progress
1. DONE: pthread_create, pthread_self, pthread_join
2. DONE: pthread_mutex_init, pthread_mutex_destroy, pthread_mutex_lock, pthread_mutex_unlock, pthread_mutex_trylock
3. DONE: pthread_cond_*
4. DONE: interposing
5. DONE: pthread_attr_* pthread_mutex_attr_*
